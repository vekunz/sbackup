all: clear linux_amd64 linux_386

clear:
	rm -R -f bin

linux_amd64:
	GOOS=linux GOARCH=amd64 go build -o bin/linux_amd64/sbackup ./src
	chmod +x bin/linux_amd64/sbackup

linux_386:
	GOOS=linux GOARCH=386 go build -o bin/linux_386/sbackup ./src
	chmod +x bin/linux_386/sbackup
