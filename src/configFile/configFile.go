package configFile

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Compression int64

const (
	None Compression = iota
	Zip
	TarGz
)

type Configuration struct {
	BaseDestination string
	Backups         map[string]*BackupConfiguration
}

type BackupConfiguration struct {
	Source      string
	KeepLast    int
	Compression Compression
}

type configurationReader struct {
	config            *Configuration
	currentLineNumber int
	currentSection    string
}

func ParseConfigFile(path string) (*Configuration, error) {
	configReader := &configurationReader{
		config: &Configuration{
			BaseDestination: "",
			Backups:         make(map[string]*BackupConfiguration),
		},
		currentLineNumber: 0,
		currentSection:    "",
	}

	if path == "" {
		path = "/etc/sbackup.conf"
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		text := scanner.Text()
		err := configReader.parseLine(text)
		if err != nil {
			return nil, err
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	err = validate(configReader.config)
	if err != nil {
		return nil, err
	}

	return configReader.config, nil
}

func (reader *configurationReader) parseLine(line string) error {
	reader.currentLineNumber++

	if line == "" || line[:1] == ";" || line[:1] == "#" {
		return nil
	}

	if line[:1] == "[" {
		if line[len(line)-1:] != "]" {
			return errors.New(fmt.Sprintf("Invalid section header on line %v: %v", reader.currentLineNumber, line))
		}
		header := line[1 : len(line)-1]
		if header == "" {
			return errors.New(fmt.Sprintf("Section header cannot be empty on line %v", reader.currentLineNumber))
		}
		reader.currentSection = header
		reader.config.Backups[header] = &BackupConfiguration{
			Source:      "",
			KeepLast:    0,
			Compression: None,
		}
		return nil
	}

	equalIndex := strings.Index(line, "=")
	if equalIndex == -1 {
		return errors.New(fmt.Sprintf("Line %v is invalid", reader.currentLineNumber))
	}

	key := line[:equalIndex]
	value := line[equalIndex+1:]

	if reader.currentSection == "" {
		switch key {
		case "baseDestination":
			reader.config.BaseDestination = value
		default:
			return errors.New(fmt.Sprintf("Invalid paramter '%v' on line %v in global section", key, reader.currentLineNumber))
		}
	} else {
		switch key {
		case "source":
			reader.config.Backups[reader.currentSection].Source = value
		case "keepLast":
			intValue, err := strconv.Atoi(value)
			if err != nil {
				return errors.New(fmt.Sprintf("Invalid value '%v' on line %v: integer expected", value, reader.currentLineNumber))
			}
			reader.config.Backups[reader.currentSection].KeepLast = intValue
		case "compression":
			switch value {
			case "zip":
				reader.config.Backups[reader.currentSection].Compression = Zip
			case "targz":
				reader.config.Backups[reader.currentSection].Compression = TarGz
			default:
				return errors.New(fmt.Sprintf("Invalid value '%v' on line %v: either 'true' or 'false' expected", value, reader.currentLineNumber))
			}
		default:
			return errors.New(fmt.Sprintf("Invalid paramter '%v' on line %v in section '%v'", key, reader.currentLineNumber, reader.currentSection))
		}
	}

	return nil
}

func validate(config *Configuration) error {
	if config.BaseDestination == "" {
		return errors.New("missing value for mandatory field 'baseDestination'")
	}

	for key, backup := range config.Backups {
		if backup.Source == "" {
			return errors.New(fmt.Sprintf("missing value for mandatory field 'source' in backup '%v'", key))
		}

		if backup.KeepLast < 0 {
			return errors.New(fmt.Sprintf("field 'keepLast' cannot be negativ in backup '%v'", key))
		}
	}

	return nil
}
