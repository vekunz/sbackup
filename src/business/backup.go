package business

import (
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sbackup/src/configFile"
	"strings"
	"time"
)

func Backup(name string, config *configFile.Configuration) error {
	configuration := config.Backups[name]

	log.Printf("Backup '%s'", name)

	currentTime := time.Now().UTC().Format(time.RFC3339)

	var sourcePath string
	var destinationPath string

	if configuration.Compression != configFile.None {
		destinationPath = fmt.Sprintf("%v/%v", config.BaseDestination, name)
		log.Printf("Compress folder '%s' and copy to '%s'", configuration.Source, destinationPath)

		dir, err := ioutil.TempDir("", "sbackup_"+name)
		if err != nil {
			return err
		}
		defer os.RemoveAll(dir)

		switch configuration.Compression {
		case configFile.Zip:
			log.Println("Create zip file")
			zipFile := fmt.Sprintf("%s/%s.zip", dir, currentTime)
			err = zipDirectory(configuration.Source, zipFile)
			if err != nil {
				return err
			}
		case configFile.TarGz:
			log.Println("Create tar.gz file")
			tarGzFile := fmt.Sprintf("%s/%s.tar.gz", dir, currentTime)
			err = tarGzDirectory(configuration.Source, tarGzFile)
			if err != nil {
				return err
			}
		}

		sourcePath = dir
	} else {
		destinationPath = fmt.Sprintf("%v/%v/%v", config.BaseDestination, name, currentTime)
		log.Printf("Copy folder '%s' to '%s'", configuration.Source, destinationPath)

		sourcePath = configuration.Source
	}

	log.Print("Upload data")
	err := execCommand(true, "rclone", "copy", sourcePath, destinationPath).Run()
	if err != nil {
		return err
	}

	log.Printf("Finished backup '%s'", name)
	return nil
}

func zipDirectory(directory string, outputFile string) error {
	file, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer file.Close()
	zipFile := zip.NewWriter(file)
	defer zipFile.Close()
	err = filepath.Walk(directory, func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.HasSuffix(filePath, ".DS_Store") {
			return nil
		}

		relPath := strings.TrimPrefix(filePath, filepath.Dir(directory))
		relPath = strings.TrimPrefix(relPath, "/")

		if info.IsDir() {
			_, err = zipFile.Create(relPath + "/")
			if err != nil {
				return err
			}
			return nil
		}

		fsFile, err := os.Open(filePath)
		if err != nil {
			return err
		}
		defer fsFile.Close()

		stat, err := fsFile.Stat()
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(stat)
		if err != nil {
			return err
		}

		header.Name = relPath
		header.Method = zip.Deflate

		writer, err := zipFile.CreateHeader(header)
		if err != nil {
			return err
		}

		_, err = io.Copy(writer, fsFile)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}

func tarGzDirectory(directory string, outputFile string) error {
	file, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer file.Close()

	gzipWriter := gzip.NewWriter(file)
	defer gzipWriter.Close()

	tarWriter := tar.NewWriter(gzipWriter)
	defer tarWriter.Close()

	err = filepath.Walk(directory, func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.HasSuffix(filePath, ".DS_Store") {
			return nil
		}

		relPath := strings.TrimPrefix(filePath, filepath.Dir(directory))
		relPath = strings.TrimPrefix(relPath, "/")

		if info.IsDir() {
			header := &tar.Header{
				Name:    relPath + "/",
				Mode:    int64(info.Mode()),
				ModTime: info.ModTime(),
			}
			err = tarWriter.WriteHeader(header)
			if err != nil {
				return err
			}

			return nil
		}

		fsFile, err := os.Open(filePath)
		if err != nil {
			return err
		}
		defer fsFile.Close()

		header := &tar.Header{
			Name:    relPath,
			Size:    info.Size(),
			Mode:    int64(info.Mode()),
			ModTime: info.ModTime(),
		}

		err = tarWriter.WriteHeader(header)
		if err != nil {
			return err
		}

		_, err = io.Copy(tarWriter, fsFile)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
