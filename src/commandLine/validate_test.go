package commandLine

import (
	"strings"
	"testing"
)

func TestParseCommandLineValidateWithoutFlags(t *testing.T) {
	args := []string{"exe", "validate"}

	cmd, err := ParseCommandLine(args)

	if err != nil {
		t.Errorf("err should be nil, but is not: %v", err)
	}

	if cmd == nil {
		t.Errorf("cmd should be set, but is nil")
	}

	if cmd.Command != Validate {
		t.Errorf("cmd.Command should be 'Validate', but is not: %v", cmd.Command)
	}

	if cmd.Configuration != "/etc/sbackup.conf" {
		t.Errorf("cmd.Configuration should be default path, but is: %v", cmd.Configuration)
	}
}

func TestParseCommandLineValidateWithUnknownFlag(t *testing.T) {
	args := []string{"exe", "validate", "--unknown"}

	cmd, err := ParseCommandLine(args)

	if cmd != nil {
		t.Errorf("cmd should not be set, but is: %v", cmd)
	}

	if err == nil {
		t.Error("err should not be nil, but is")
	}

}

func TestParseCommandLineValidateWithConfigFlag(t *testing.T) {
	variants := [][]string{
		{"-c", "something.conf"},
		{"--configuration=something.conf"},
	}

	for _, variant := range variants {
		t.Run(strings.Join(variant, " "), func(t *testing.T) {
			args := []string{"exe", "validate"}
			args = append(args, variant...)

			cmd, err := ParseCommandLine(args)

			if err != nil {
				t.Errorf("err should be nil, but is not: %v", err)
			}

			if cmd == nil {
				t.Errorf("cmd should be set, but is nil")
			}

			if cmd.Configuration != "something.conf" {
				t.Errorf("cmd.Configuration should be 'something.conf', but is: %v", cmd.Configuration)
			}
		})
	}
}
