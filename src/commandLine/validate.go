package commandLine

import (
	"errors"
	"fmt"
)

func parseValidateCommandLine(cmd *CommandLine, args map[string]string) error {
	if helpParameterAnywhere(args) {
		cmd.ShowHelp = true
		return nil
	}

	for key, value := range args {
		switch key {
		case "-c", "--configuration":
			cmd.Configuration = value
		default:
			return errors.New(fmt.Sprintf("Invalid parameter %v", key))
		}
	}

	return nil
}
