package commandLine

import (
	"errors"
	"fmt"
	"strings"
)

type Command int64

const (
	None Command = iota
	Backup
	Clean
	Help
	Validate
)

type CommandLine struct {
	Command       Command
	ShowHelp      bool
	Configuration string
	Parameters    map[string]string
}

func ParseCommandLine(args []string) (*CommandLine, error) {
	if len(args) == 1 {
		return nil, nil
	}

	cmd := &CommandLine{
		Command:       None,
		ShowHelp:      false,
		Configuration: "/etc/sbackup.conf",
		Parameters:    make(map[string]string),
	}

	var err error

	switch args[1] {
	case "backup":
		cmd.Command = Backup
		err = parseBackupCommandLine(cmd, parseArgs(args[2:]))
	case "clean":
		cmd.Command = Clean
		err = parseCleanCommandLine(cmd, parseArgs(args[2:]))
	case "help", "--help", "-h":
		cmd.Command = Help
		cmd.ShowHelp = true
	case "validate":
		cmd.Command = Validate
		err = parseValidateCommandLine(cmd, parseArgs(args[2:]))
	default:
		return nil, errors.New(fmt.Sprintf("Unknown command '%v'", args[1]))
	}

	if err != nil {
		return nil, err
	}

	return cmd, nil
}

func helpParameterAnywhere(args map[string]string) bool {
	for key := range args {
		if key == "--help" || key == "-h" {
			return true
		}
	}
	return false
}

func parseArgs(args []string) map[string]string {
	result := make(map[string]string)

	for i := 0; i < len(args); i++ {
		arg := args[i]

		key := ""
		value := ""

		if len(arg) >= 2 && arg[:2] == "--" {
			equalIndex := strings.Index(arg, "=")
			if equalIndex == -1 {
				equalIndex = len(arg)
			}
			key = arg[:equalIndex]
			if equalIndex != len(arg) {
				value = arg[equalIndex+1:]
			}
		} else if len(arg) >= 1 && arg[:1] == "-" {
			key = arg

			if len(args) > i+1 && len(args[i+1]) >= 1 && args[i+1][:1] != "-" {

				value = args[i+1]
				i++
			}
		} else {
			key = arg
		}

		result[key] = value
	}

	return result
}
